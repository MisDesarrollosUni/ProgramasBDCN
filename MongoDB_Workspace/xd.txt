// 3. Cantidad de comentarios que han recibido las películas estrenadas entre 1926 y 1931.
db.movies.find()
db.movies.aggregate(
    {
        $match: {
          $and:[
                {
                    'year':{
                         $gte: 1926,
                         $lte: 1931
                    }
                },
                {
                   'num_mflix_comments':{
                        $exists: true
                   }
                }
            ]
        }
    },
    {
        $project:{
            'cantidad_comentarios':{
                $sum: '$num_mflix_comments'
            }
        }
    },{
        $group:{
            _id: 'cantidad_comentarios',
            "total_comentarios":{
                $sum: '$cantidad_comentarios'
            }
        }
    }
)
