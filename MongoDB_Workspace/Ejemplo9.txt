//createView(nombre, coleccion, pipeline, opciones)
//opciones <- collation(objeto): idioma, ordenamiento (caracteres)
//Una vista se observa como una coleccion
//db.vista.find()
// No se puede usar $elemMatch, $slice
//Cursor
// No se puede usar .mapReduce(), $text, $geoNear
//No se pueden renombrar 
//No se pueden hacer inserciones, eliminaciones o actualizaciones
db.createView("totalAmountOfSales", "sales", [
    { $unwind: "$items" },
    { $group: { _id: "$saleDate", cantidad: { $sum: { $multiply: ["$items.price", "$items.quantity"] } } } }
]
)
//Promedio de cocinas por restaurantes 
db.createView("AVG", "restaurants", [
    { $unwind: "$grades" },
    { $group: { _id: "$cousine", promedio: { $avg: "$grades.score" } } },
    { $match: { promedio: { $gte: 0 } } },
    { $sort: { promedio: -1, _id: 1 } }
])
//Indices
//_id 
//Crear nodos empezando por la primer letra
//A,B,C, etc...
//Hace lento las operaciones de update, create o delete
//db.collection.createIndex({campo:1, campo:-1})
db.restaurants.find({ cuisine: "Pizza" })
db.restaurants.createIndex(
    { "cuisine": 1 },
)
// Indice simple y ordenamiento (Tiene un campo y el ordenamiento)
//Si se va a crear un indice en una coleccion grande (millones de docs)
//Se crea al inicio
//Se crea al final
//Los crea por DBA y no por el programador
//Indice compuesto (Tiene mas de un campo con su respectivo ordenamiento)
db.restaurants.createIndex(
    { "cuisine": 1, "borough": 1 }
)
db.people.insert({ name: null, lastName: null })
db.people.createIndex({ name: 1, lastName: 1 }, { unique: true })
db.people.find({ name: null })
//¿Pueden incluirse personas sin nombre y/o apellidos?
//No, el indice indica que son necesarios
//Si, porque los valores nulos no cuentan para el indice
//Si, pero solo una vez (Esta es la correcta, se crea uno solo nada mas)
//Indices textuales
//Hechos para buscar campos con mucho texto
db.listingsAndReviews.findOne()
db.listingsAndReviews.createIndex({ "description": 1 })
//Indice 2DSPHERE
//Requiere un arreglo que contenga latitud y longitud (valores numericos, decimales)
db.listingsAndReviews.createIndex(
    { "adress.location": "2dsphere" }
)
//COLLSCAN revisa toda la coleccion
//IXSCAN usa un indice y es mas rapido (busqueda frecuentes)