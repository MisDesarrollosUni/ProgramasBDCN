package mx.edu.utez.controller;

import mx.edu.utez.model.Alumno;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.sql.Date;
import java.util.List;

public class Registro {
    public static void main(String[] args) {

        /* TODO Conexión
            Patron de diseño factory, crear una instancia espeficica y pueda manipuarla completamente
            Majenador de entidades en la DB y que archivo debe gestionar, si existe lo abre y si no lo crea
         */
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("object.odb");

        //Manipular la instancia, por cada usuario conectado
        EntityManager em = emf.createEntityManager();


        /* TODO Insersión
            Lo quieras ejecutar va dentro de la transsacción y puedes hacer mas cosas
         */

        /*em.getTransaction().begin();
        Alumno alumno = new Alumno("20193tn070", "Hector Saldaña", Date.valueOf("2001-02-05"));
        em.persist(alumno); // Guardaría el objeto
        em.getTransaction().commit();*/

         /*
             Para visualizar la DB, ejecutas el explorer.jar de ObjectDB
             y cerrar la conexión para ejeuctar cosas aquí, no es multi Hilo
         */


        /*
            TODO Consultar
             JPQL - Java Persistence Query Language
             1. Forma
                Similiar a sentencias SQL

                Devulve todos los objetos de tipo alumno con un alias
         */
        Query q = em.createQuery("SELECT a FROM Alumno a ");

        // Recupera el primero resultado
        //q.getFirstResult(); // quer.next() de db4o

        //Recupera todos los objetos de la consulta
        List<Alumno> alumnos = q.getResultList();
        for(Alumno a: alumnos){
            System.out.println(a);
        }
    }
}
