package mx.edu.utez.controller;

import mx.edu.utez.model.Alumno;
import mx.edu.utez.model.Casa;
import mx.edu.utez.model.Materia;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

   /* private static EntityManagerFactory emf = null;
    private static EntityManager em =null;

    public static void iniciar(){
        Map<String, String> properties = new HashMap<String, String>();
        properties.put("javax.persistence.jdbc.user", "admin");
        properties.put("javax.persistence.jdbc.password", "admin");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("objectdb://localhost:6136/object.odb", properties);
        em = emf.createEntityManager();
    }*/


    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("object.odb");
    private static EntityManager em = emf.createEntityManager();

    public static void main(String[] args) {

        //iniciar();
        /*consultarTodosAlumnos();
        System.out.println();
        System.out.println(consultarSiExisteAlumno("ihj"));*/
        // usandoSequenceStrategy();

        //consultarTodasMaterias();
        //cambiarNombreMateria("BD", "BASE");
        //consultarTodosAlumnos();

        //crearMateriasYAlumnos();

        consultarTodosAlumnos();

    }

    public static void cambiarNombreMateria(String clave, String nombre) {
        TypedQuery<Materia> q = em.createQuery("SELECT m FROM Materia m WHERE m.clave = :mat", Materia.class);
        q.setParameter("mat", clave);
        Materia materia = q.getSingleResult();
        em.getTransaction().begin();
        materia.setClave(nombre);
        em.getTransaction().commit();
    }

    public static void crearMateriasYAlumnos() {
        Alumno a1 = new Alumno("01", "Hector", Date.valueOf("2001-5-8"));
        Alumno a2 = new Alumno("02", "Luis", Date.valueOf("2001-5-8"));
        Alumno a3 = new Alumno("03", "Mario", Date.valueOf("2001-5-8"));
        Alumno a4 = new Alumno("04", "Alberto", Date.valueOf("2001-5-8"));
        Alumno a5 = new Alumno("05", "Hugo", Date.valueOf("2001-5-8"));
        Alumno a6 = new Alumno("06", "Paty", Date.valueOf("2001-5-8"));
        Alumno a7 = new Alumno("07", "Mariano", Date.valueOf("2001-5-8"));
        Alumno a8 = new Alumno("08", "Patricio", Date.valueOf("2001-5-8"));
        Alumno a9 = new Alumno("09", "Maria", Date.valueOf("2001-5-8"));
        Alumno a10 = new Alumno("10", "Juana", Date.valueOf("2001-5-8"));

        List<Alumno> alumnos1 = new ArrayList<>();
        List<Alumno> alumnos2 = new ArrayList<>();

        alumnos1.add(a1);
        alumnos1.add(a2);
        alumnos1.add(a3);
        alumnos1.add(a4);
        alumnos1.add(a5);

        alumnos2.add(a6);
        alumnos2.add(a7);
        alumnos2.add(a8);
        alumnos2.add(a9);
        alumnos2.add(a10);

        Materia m1 = new Materia("BD", "Base de datos", alumnos1);
        Materia m2 = new Materia("IN", "Integradora", alumnos2);

        em.getTransaction().begin();
        em.persist(a1);
        em.persist(a2);
        em.persist(a3);
        em.persist(a4);
        em.persist(a5);
        em.persist(a6);
        em.persist(a7);
        em.persist(a8);
        em.persist(a9);
        em.persist(a10);
        em.persist(m1);
        em.persist(m2);
        em.getTransaction().commit();
    }

    public static void usandoSequenceStrategy() {
        em.getTransaction().begin();
        Casa casa1 = new Casa("De Mari");
        Casa casa2 = new Casa("De Carmen");
        Casa casa3 = new Casa("De Angeles");
        em.persist(casa1);
        em.persist(casa2);
        em.persist(casa3);
        em.getTransaction().commit();
    }

    public static boolean consultarSiExisteAlumno(String nombre) {
        boolean flag = false;
        try {
            em.getTransaction().begin();
            TypedQuery<Alumno> query = em.createQuery("SELECT a FROM Alumno a WHERE a.nombre = :n ", Alumno.class);
            query.setParameter("n", nombre);
            Alumno alumno = query.getSingleResult();
            if (alumno != null) flag = true;
        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            em.getTransaction().commit();
        }
        return flag;
    }

    public static void consultarTodosAlumnos() {
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT a FROM Alumno a");
        List<Alumno> alumnos = query.getResultList();
        em.getTransaction().commit();
        for (Alumno a : alumnos) {
            System.out.println(a);
        }
    }


    public static void consultarTodasMaterias() {
        em.getTransaction().begin();
        TypedQuery<Materia> query = em.createQuery("SELECT m FROM Materia m", Materia.class);
        List<Materia> materias = query.getResultList();
        for (Materia m : materias)
            System.out.println(m);
        em.getTransaction().commit();
    }


    public static void insertarAlumnos() {
        em.getTransaction().begin();
        Alumno a1 = new Alumno("001", "Marcos", Date.valueOf("2001-1-5"));
        Alumno a2 = new Alumno("002", "Juan", Date.valueOf("2001-1-5"));
        Alumno a3 = new Alumno("003", "Diego", Date.valueOf("2001-1-5"));
        Alumno a4 = new Alumno("004", "Hector", Date.valueOf("2001-1-5"));
        Alumno a5 = new Alumno("005", "Luis", Date.valueOf("2001-1-5"));
        Alumno a6 = new Alumno("006", "Francisco", Date.valueOf("2001-1-5"));
        em.persist(a1);
        em.persist(a2);
        em.persist(a3);
        em.persist(a4);
        em.persist(a5);
        em.persist(a6);
        em.getTransaction().commit();
    }
}
