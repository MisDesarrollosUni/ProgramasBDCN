package mx.edu.utez.model;

import javax.persistence.*;
import java.util.List;

@Entity(name = "miComputadora")
public class Computadora {

    /**
     * * Campos persistentes
     * ? Son los tipos de datos primitivos, wrapper, creados por el usuario
     * ? de tipo Date, Math, Colleciones, ENUM y arrays
     */


    /**
     * * Campos transistorios
     * ? Estos campos no se almacenan en la base de datos
     * ? o no se miran reflejados ahí
     */
    static int componentes1; // ! No sube static
    final int componentes2 = 0; // ! final
    transient int componentes3; // !transient
    @Transient // ! ni con @Transient porque no son persistentes
    int componentes4;

     /*
    -------------------------------------------------------------
     */

    /**
     * ? Con esto, se le dice si el dato puede ser o no opción
     * ? si no es opcional, arrojara una excepción
     */
    @Basic (optional = false)
    private String nombre;

    /*
    -------------------------------------------------------------
     */

    /**
     * * Campos embebidos
     * ? Son aquellas clases creadas por el usuario
     */
    @Embedded
    private List<Alumno> alumnos;

     /*
    -------------------------------------------------------------
     */

    /**
     * * Valores autogenerados
     * ? Se dice que las bases de datos pueden contener ID que se autogeneran
     * ? y se hace a través de @GeneratedValue with the AUTO strategy:
     */

    // Estas dos formas de declarar un id son equivalentes
    @Id @GeneratedValue(strategy= GenerationType.AUTO) long id;
    @Id @GeneratedValue long idEquivalente;

     /*
    -------------------------------------------------------------
     */



}
