package mx.edu.utez.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Materia implements Serializable {
    @Id @GeneratedValue
    private long id;

    private String clave;
    private String nombre;

    @OneToMany
    private List<Alumno> alumnos;


    public Materia(String clave, String nombre, List<Alumno> alumnos) {
        this.clave = clave;
        this.nombre = nombre;
        this.alumnos = alumnos;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Alumno> getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(List<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

    @Override
    public String toString() {
        return "Materia{" +
                "id=" + id +
                ", clave='" + clave + '\'' +
                ", nombre='" + nombre + '\'' +
                ", alumnos=" + alumnos +
                '}';
    }
}
