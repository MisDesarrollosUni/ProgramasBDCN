package mx.edu.utez.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/*
   TODO
    JPA - Java Persistence API
    ORM - Object Relational Mapping
    Es base de los ORM porque mapea el objecto
 */

/*
    Decirle que nuestra entidad sera Alumno, para que sepa que de aquí se van a insertar
*/
@Entity
public class Alumno implements Serializable {

    /*
        Obtiene un ID de ObjectID, por eso se crea,
        Estas anotaciones, son para llave primaria incremental, si fuera el caso de DB relacional y no relacional
        y tambien se le dice que será el ID de esta entidad
     */

    @Id // Sera ID
    @GeneratedValue // Será autoincrementable
    private long id;
    @Basic(optional = false)
    private String matricula;
    @Basic(optional = false)
    private String nombre;
    @Basic(optional = false)
    private Date fechaNacimiento;

    public Alumno() { }

    //Para el long id, no se crea un constructor
    public Alumno(String matricula, String nombre, Date fechaNacimiento) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        return "Alumno{" +
                "id=" + id +
                ", matricula='" + matricula + '\'' +
                ", nombre='" + nombre + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                '}';
    }
}
