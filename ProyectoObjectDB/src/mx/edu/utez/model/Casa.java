package mx.edu.utez.model;

import javax.persistence.*;

@Entity
@SequenceGenerator(name = "casita", initialValue = 25, allocationSize = 30)
public class Casa {
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "casita")
    @Id private long id;
    private String nombre;
    @Basic(optional = true)
    private int cuartos;


    public Casa(String nombre) {
        this.nombre = nombre;
    }

    public int getCuartos() {
        return cuartos;
    }

    public void setCuartos(int cuartos) {
        this.cuartos = cuartos;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
