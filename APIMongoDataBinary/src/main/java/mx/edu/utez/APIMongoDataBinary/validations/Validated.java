package mx.edu.utez.APIMongoDataBinary.validations;

import mx.edu.utez.APIMongoDataBinary.document.Profile;

public class Validated {
    private final String CARACTERES = "áéíóúÁÉÍÓÚabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789. ";

    public boolean validation(Profile profile){
        boolean flag = false;
        try {
            if(validarCampoTexto(profile.getName()) && validarCampoTexto(profile.getDescription())){
                flag = true;
            }
        }catch (Exception e){
            e.getStackTrace();
        }finally {
            return flag;
        }
    }

    private boolean validarCampoTexto(String texto) {
        boolean flag = true;
        if (!texto.isEmpty()) {
            for (var i = 0; i < texto.length(); i++) {
                if (CARACTERES.indexOf(texto.charAt(i)) == -1) {
                    flag = false;
                    break;
                }
            }
        }
        return flag;
    }
}
