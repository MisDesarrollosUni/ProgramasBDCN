package mx.edu.utez.APIMongoDataBinary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMongoDataBinaryApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiMongoDataBinaryApplication.class, args);
	}

}
