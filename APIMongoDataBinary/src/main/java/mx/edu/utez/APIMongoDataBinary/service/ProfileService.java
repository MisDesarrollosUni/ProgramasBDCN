package mx.edu.utez.APIMongoDataBinary.service;

import mx.edu.utez.APIMongoDataBinary.document.Profile;
import mx.edu.utez.APIMongoDataBinary.repository.ProfileRepository;
import mx.edu.utez.APIMongoDataBinary.validations.Validated;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class ProfileService {

    @Autowired
    private ProfileRepository profileRepository;
    private Validated validated = new Validated();

    public Optional<Profile> insertOne(MultipartFile file, Profile profile) throws IOException {
        if (validated.validation(profile)) {
            profile.setImage(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
            profileRepository.save(profile);
            return findOne(profile.getId());
        }
        return Optional.empty();
    }

    public Optional<Profile> findOne(String id) {
        return profileRepository.findById(id);
    }

    public List<Profile> findAll(){
        return  profileRepository.findAll();
    }
}
