package mx.edu.utez.APIMongoDataBinary.repository;

import mx.edu.utez.APIMongoDataBinary.document.Profile;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface ProfileRepository extends MongoRepository<Profile, Serializable> {

}
