package mx.edu.utez.APIMongoDataBinary.document;

import com.mongodb.lang.NonNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "profile")
@Data
@NoArgsConstructor
@ToString
@AllArgsConstructor
@TypeAlias(value = "My Profile Class")
public class Profile implements Serializable {
    @Id
    @NonNull
    private String id;
    private String name;
    private Binary image;
    private String description;
}
