package mx.edu.utez.APIMongoDataBinary.controller;

import mx.edu.utez.APIMongoDataBinary.document.Profile;
import mx.edu.utez.APIMongoDataBinary.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/mongo")
@CrossOrigin(origins = "http://127.0.0.1:5500")
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @PostMapping("/add")
    public ResponseEntity<Optional<Profile>> saveProfile(
            @RequestParam("image") MultipartFile file,
            @RequestParam("name") String name,
            @RequestParam("description") String description) {
        try {
            Profile profile = new Profile();
            profile.setName(name);
            profile.setDescription(description);
            Optional<Profile> result = profileService.insertOne(file, profile);
            return new ResponseEntity<>(result,HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public List<Profile> getAll(){
        return profileService.findAll();
    }

}
