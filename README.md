# 5C | ProgramasBDCN
## Base de Datos para Cómputo en la Nube ⚙️ ☁
> Saldaña Espinoza Hector - Desarrollo de Software
>

### Contenido 🚀
* [APIMongoDBJava](https://github.com/HectorSaldes/ProgramasBDCN/tree/master/APIMongoDBJava)  - API con Mongo Ejemplo
* [APIMongoDataBinary](https://github.com/HectorSaldes/ProgramasBDCN/tree/master/APIMongoDataBinary)  - API con Mongo Subida de Imagenes
* [Agencia](https://github.com/HectorSaldes/ProgramasBDCN/tree/master/Agencia)  - API con Mongo y Propiedades de Conexión
* [EjemploObjectDB](https://github.com/HectorSaldes/ProgramasBDCN/tree/master/EjemploObjectDB)  - Uso de ObjectDB
* [MongoDB_Workspace](https://github.com/HectorSaldes/ProgramasBDCN/tree/master/MongoDB_Workspace)  - Consultas en Mongo DB
* [MyMongoStatic](https://github.com/HectorSaldes/ProgramasBDCN/tree/master/MyMongoStatic)  - MongoDB en Azure
* [ObjectDB - CRUD](https://github.com/HectorSaldes/ProgramasBDCN/tree/master/ObjectDB%20-%20CRUD)  - ObjectDb Curd
* [ProyectoBaseDeDatos](https://github.com/HectorSaldes/ProgramasBDCN/tree/master/ProyectoBaseDeDatos)  - No directory
* [ProyectoCrudEscuela](https://github.com/HectorSaldes/ProgramasBDCN/tree/master/ProyectoCrudEscuela)  - No directory
* [ProyectoObjectDB](https://github.com/HectorSaldes/ProgramasBDCN/tree/master/ProyectoObjectDB)  - Ejemplo de ObjectDb
* [SGI-Store](https://github.com/HectorSaldes/ProgramasBDCN/tree/master/SGI-Store)  -CRUD db4o (Fue mi tortura)
* [SaldañaEspinoza](https://github.com/HectorSaldes/ProgramasBDCN/tree/master/SaldañaEspinoza)  -CRUD db4o (Fue mi tortura)
