const CARACTERES = 'áéíóúÁÉÍÓÚabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ.0123456789 ';

function validarCampo(text) {
    var flag = true;
    if (!(text === "")) {
        for (var i = 0; i < text.length; i++) {
            if (CARACTERES.indexOf(text.charAt(i)) == -1) {
                flag = false;
                break;
            }
        }
    } else {
        flag = false;
    }
    return flag;
}

module.exports = validarCampo