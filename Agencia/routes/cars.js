const express = require('express')
const router = express.Router()
const mongojs = require('mongojs')
const db = mongojs('mongodb+srv://m001-student:m001-mongodb-basics@sandbox.vbgsi.mongodb.net/agency')
const validarCampo = require('../validar')

router.get('/cars', (req, res) => {
    db.cars.find((err, cars) => {
        if (err) {
            res.send(err)
        }
        res.json(cars)
    })
})

router.get('/car/:id', (req, res) => {
    db.cars.findOne({ _id: mongojs.ObjectId(req.params.id) }, (err, cars) => {
        if (err) {
            res.send(err)
        }
        res.json(cars)
    })
})

router.post('/car', (req, res) => {
    let car = req.body
    if (validarCampo(car.model) && validarCampo(car.brand) && validarCampo(car.year) && validarCampo(car.color)) {
        db.cars.insert(car, (err, resul) => {
            if (err) {
                res.send(err)
            }
            res.json(resul)
        })
    } else {
        res.status(400);
        res.json({ "error": 'Algunos de los valores enviados están vacios o continen cosas raras .-.' })
    }
})

router.put('/car', (req, res) => {
    let car = req.body
    if (validarCampo(car.id) && validarCampo(car.model) && validarCampo(car.brand) && validarCampo(car.year) && validarCampo(car.color)) {
        db.cars.update({ _id: mongojs.ObjectId(car.id) }, { $set: { model: car.model, brand: car.brand, year: parseInt(car.year), color: car.color } },
            (err, resul) => {
                if (err) {
                    res.send(err)
                }
                res.json(resul)
            })
    } else {
        res.status(400);
        res.json({ "error": 'Algunos de los valores enviados están vacios o continen cosas raras .-.' })
    }
})

router.delete('/car/:id', (req, res) => {
    db.cars.remove({ _id: mongojs.ObjectId(req.params.id) }, (err, resul) => {
        if (err) {
            res.send(err)
        }
        res.json(resul)
    })
})

module.exports = router