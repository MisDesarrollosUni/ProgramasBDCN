import controller.Modulos;

import java.util.Scanner;

public class Main {

    final static String CARACTERES = "áéíóúÁÉÍÓÚabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ ";
    static Scanner  leer = new Scanner(System.in);
    public static void main(String[] args) {
        /*Modulos modulos = new Modulos();
        modulos.menu();*/
        String e = validarNombre();
        System.out.println(e);
    }

    public static String validarNombre(){
        leer.useDelimiter("\n");
        String texto;
        boolean flag;
        do{
            flag = false;
            System.out.print("Ingrese el nombre: ");
            texto = leer.next();
            if (!texto.isEmpty()){
                for (int i = 0; i < texto.length(); i++) {
                    if (CARACTERES.indexOf(texto.charAt(i)) == -1) {
                        flag = true;
                        break;
                    }
                }
            }else {
                System.out.println("* El nombre no puede estar vacío");
            }
        }while (flag);
        return texto;
    }
}
