package model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Empleado implements Serializable {
    @Id @GeneratedValue
    private long id;
    private String clave;
    private String nombre;

    public Empleado() { }

    public Empleado(String clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }



    @Override
    public String toString() {
        return "Empleado{" +
                "id=" + id +
                ", clave='" + clave + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
