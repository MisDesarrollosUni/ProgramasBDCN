package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Departamento implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String clave;
    private String area;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Empleado> empleados;

    public Departamento() { }

    public Departamento(String clave, String area) {
        this.clave = clave;
        this.area = area;
    }

    public Departamento(String clave, String area, List<Empleado> empleados) {
        this.clave = clave;
        this.area = area;
        this.empleados = empleados;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }

    @Override
    public String toString() {
        return "Departamento{" +
                "id=" + id +
                ", clave='" + clave + '\'' +
                ", empleados=" + empleados +
                '}';
    }
}
