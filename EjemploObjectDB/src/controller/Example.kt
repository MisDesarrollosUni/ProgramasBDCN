package controller

import java.util.*


fun main(){
    getDayOfTheWeek()
}

private fun getDayOfTheWeek() {
    val days = arrayOf("DOMINGO", "LUNES", "MARTES", "MIÉRCOLES", "JUEVES", "VIERNES", "SÁBADO")
    val today = Date()
    var numericDay:Int
    val cal = Calendar.getInstance()
    cal.time = today
    numericDay = cal[Calendar.DAY_OF_WEEK]
    println(days[--numericDay])
}