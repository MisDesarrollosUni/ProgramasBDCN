package controller;

import model.Departamento;
import model.Empleado;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Modulos {
    private Scanner leer = new Scanner(System.in);
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("base.odb");
    private EntityManager em = emf.createEntityManager();

    public void menu() {
        leer.useDelimiter("\n");
        int opc, opcInterno;
        String clave, nombre, area, claveDepa;
        do {
            System.out.println("\n****************************************");
            System.out.println("\tEMPLEADOS MENÚ");
            System.out.println("1. Insertar\n2. Consultar todos\n3. Eliminar\n4. Modificar\n");
            System.out.println("\tDEPARTAMENTOS MENÚ");
            System.out.println("5. Insertar\n6. Consultar todos\n7. Eliminar\n8. Modificar\n9. Agregar empleados\n10. Consultar empleados");
            System.out.println("11. Salir\n");
            System.out.print("Seleccione: ");
            opc = validarNumero();
            switch (opc) {
                case 1:
                    System.out.print("Ingrese nueva clave: ");
                    clave = leer.next();
                    if (!existeEmpleado(clave)) {
                        System.out.print("Ingresa nombre completo: ");
                        nombre = leer.next();
                        if (insertarEmpleado(new Empleado(clave, nombre))) {
                            System.out.println("\tEMPLEADO INSERTADO");
                        } else {
                            System.out.println("\tEMPLEADO NO INSERTADO");
                        }
                    } else {
                        System.out.println("\tLA CLAVE DEL EMPLEADO YA EXISTE");
                    }
                    break;
                case 2:
                    consultarTodosEmpleados();
                    break;
                case 3:
                    System.out.print("Ingrese la clave: ");
                    clave = leer.next();
                    if (existeEmpleado(clave)) {
                        System.out.print("\n¿Estás seguro de eliminarlo?\n1. SI\n2. NO\nSeleccione: ");
                        opcInterno = validarNumero();
                        switch (opcInterno) {
                            case 1:
                                if (eliminarEmpleado(clave)) {
                                    System.out.println("\tEMPLEADO ELIMIADO");
                                } else {
                                    System.out.println("\tEMPLEADO NO ELIMIADO");
                                }
                                break;
                            case 2: break;
                            default:
                                System.out.println("\tNO EXISTE SA OPCIÓN");
                        }
                    } else {
                        System.out.println("\tLA CLAVE DEL EMPLEADO NO EXISTE");
                    }
                    break;
                case 4:
                    System.out.print("Ingrese la clave: ");
                    clave = leer.next();
                    if (existeEmpleado(clave)) {
                        System.out.print("Ingrese el nuevo nombre: ");
                        nombre = leer.next();
                        if (modificarEmpleado(new Empleado(clave, nombre))) {
                            System.out.println("\tEMPLEADO MODIFICADO");
                        } else {
                            System.out.println("\tEMPLEADO NO MODIFICADO");
                        }
                    } else {
                        System.out.println("\tLA CLAVE DEL EMPLEADO NO EXISTE");
                    }
                    break;
                case 5:
                    System.out.print("Ingrese nueva clave: ");
                    clave = leer.next();
                    if (!existeDepartamento(clave)) {
                        System.out.print("Ingresa el área: ");
                        area = leer.next();
                        if (insertarDepartamento(new Departamento(clave, area, new ArrayList<>()))) {
                            System.out.println("\tDEPARTAMENTO INSERTADO");
                        } else {
                            System.out.println("\tDEPARTAMENTO NO INSERTADO");
                        }
                    } else {
                        System.out.println("\tLA CLAVE DEL DEPARTAMENTO YA EXISTE");
                    }
                    break;
                case 6:
                    consultarTodosDepartamentos();
                    break;
                case 7:
                    System.out.print("Ingrese la clave: ");
                    clave = leer.next();
                    if(existeDepartamento(clave)){
                        System.out.print("\n¿Estás seguro de eliminarlo?\n1. SI\n2. NO\nSeleccione: ");
                        opcInterno = validarNumero();
                        switch (opcInterno){
                            case 1:
                                if(eliminarDepartamento(clave)){
                                    System.out.println("\tDEPARTAMENTO ELIMIADO");
                                }else {
                                    System.out.println("\tDEPARTAMENTO NO ELIMIADO");
                                }
                                break;
                            case 2: break;
                            default:  System.out.println("\tNO EXISTE SA OPCIÓN");
                        }
                    }else {
                        System.out.println("\tLA CLAVE DEL DEPARTAMENTO NO EXISTE");
                    }
                    break;
                case 8:
                    System.out.print("Ingrese la clave: ");
                    clave = leer.next();
                    if(existeDepartamento(clave)){
                        System.out.print("Ingresa la nueva área: ");
                        area = leer.next();
                        if (modificarDepartamento(new Departamento(clave, area))){
                            System.out.println("\tDEPARTAMENTO MODIFICADO");
                        }else {
                            System.out.println("\tDEPARTAMENTO NO MODIFICADO");
                        }
                    }else {
                        System.out.println("\tLA CLAVE DEL DEPARTAMENTO NO EXISTE");
                    }
                    break;
                case 9:
                    System.out.print("Ingrese la clave del departamento: ");
                    claveDepa = leer.next();
                    if(existeDepartamento(claveDepa)){
                        System.out.print("Ingrese la clave del empleado: ");
                        clave = leer.next();
                        if(existeEmpleado(clave)){
                            if(insertarEmpleadoDepartamento(claveDepa,clave)){
                                System.out.println("\tEMPLEADO INSERTADO EN EL DEPARTAMENTO");
                            }else {
                                System.out.println("\tEMPLEADO NO INSERTADO EN EL DEPARTAMENTO");
                            }
                        }else {
                            System.out.println("\tLA CLAVE DEL EMPLEADO NO EXISTE");
                        }
                    }else{
                        System.out.println("\tLA CLAVE DEL DEPARTAMENTO NO EXISTE");
                    }
                    break;
                case 10:
                    System.out.print("Ingrese la clave del departamento: ");
                    claveDepa = leer.next();
                    if (existeDepartamento(claveDepa)){
                        consultarEmpleadosDepartamento(claveDepa);
                    }else {
                        System.out.println("\tLA CLAVE DEL DEPARTAMENTO NO EXISTE");
                    }
                    break;
                case 11:
                    em.close();
                    emf.close();
                    break;
                default:
                    System.out.println("\t----Eliga una opción válida----");
            }
        } while (opc != 11);
    }

    public void consultarEmpleadosDepartamento(String claveDep){
        TypedQuery<Departamento> q = em.createQuery("SELECT d FROM Departamento d WHERE d.clave = :c", Departamento.class);
        Departamento departamento = q.setParameter("c", claveDep).getSingleResult();
        if(departamento.getEmpleados().isEmpty()){
            System.out.println("\tNO HAY EMPLEADOS EN EL DEPARTAMENTOS AÚN");
        }else {
            for (Empleado e: departamento.getEmpleados()){
                System.out.println(e);
            }
        }
    }

    public boolean insertarEmpleadoDepartamento(String claveDep, String claveEmp){
        boolean flag = false;
        try{
            Empleado empleado = consultarEmpleado(claveEmp);
            Departamento departamento = consultarDepartamento(claveDep);
            em.getTransaction().begin();
            departamento.getEmpleados().add(empleado);
            em.getTransaction().commit();
            flag = true;
        }catch (Exception e){
            e.getMessage();
        }finally {
            return flag;
        }
    }

    public boolean modificarEmpleado(Empleado emp) {
        boolean flag = false;
        try {
            Empleado empleado = consultarEmpleado(emp.getClave());
            em.getTransaction().begin();
            empleado.setNombre(emp.getNombre());
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public boolean modificarDepartamento(Departamento dep){
        boolean flag = false;
        try {
            Departamento departamento = consultarDepartamento(dep.getClave());
            em.getTransaction().begin();
            departamento.setArea(dep.getArea());
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }

    }

    public boolean eliminarEmpleado(String clave) {
        boolean flag = false;
        try {
            Empleado empleado = consultarEmpleado(clave);
            em.getTransaction().begin();
            em.remove(empleado);
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public boolean eliminarDepartamento(String clave){
        boolean flag = false;
        try {
            Departamento departamento = consultarDepartamento(clave);
            em.getTransaction().begin();
            em.remove(departamento);
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public Departamento consultarDepartamento(String clave){
        Departamento departamento = null;
        try{
            TypedQuery<Departamento> q = em.createQuery("SELECT d FROM Departamento d WHERE d.clave = :c", Departamento.class);
            departamento = q.setParameter("c", clave).getSingleResult();
        }catch (Exception e){
            e.getMessage();
        }finally {
            return departamento;
        }
    }

    public Empleado consultarEmpleado(String clave) {
        Empleado empleado = null;
        try {
            TypedQuery<Empleado> q = em.createQuery("SELECT e FROM Empleado e WHERE e.clave = :c", Empleado.class);
            empleado = q.setParameter("c", clave).getSingleResult();
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return empleado;
        }
    }

    public void consultarTodosDepartamentos(){
        TypedQuery<Departamento> q = em.createQuery("SELECT d FROM Departamento d", Departamento.class);
        List<Departamento> departamentos = q.getResultList();
        if(departamentos.isEmpty()){
            System.out.println("\tNO HAY DEPARTAMENTOS AÚN");
        }else {
            for (Departamento d: departamentos){
                System.out.println(d);
            }
        }
    }

    public void consultarTodosEmpleados() {
        TypedQuery<Empleado> q = em.createQuery("SELECT e FROM Empleado e", Empleado.class);
        List<Empleado> empleados = q.getResultList();
        if (empleados.isEmpty()) {
            System.out.println("\tNO HAY EMPLEADOS AÚN");
        } else {
            for (Empleado e : empleados) {
                System.out.println(e);
            }
        }
    }

    private boolean insertarDepartamento(Departamento departamento) {
        boolean flag = false;
        try {
            em.getTransaction().begin();
            em.persist(departamento);
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public boolean insertarEmpleado(Empleado empleado) {
        boolean flag = false;
        try {
            em.getTransaction().begin();
            em.persist(empleado);
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public boolean existeEmpleado(String clave) {
        boolean flag = false;
        try {
            TypedQuery<Empleado> q = em.createQuery("SELECT e FROM Empleado e WHERE e.clave = :c", Empleado.class);
            Empleado empleado = q.setParameter("c", clave).getSingleResult();
            if (empleado != null) {
                flag = true;
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public boolean existeDepartamento(String clave) {
        boolean flag = false;
        try {
            TypedQuery<Departamento> q = em.createQuery("SELECT d FROM Departamento d WHERE d.clave = :c", Departamento.class);
            Departamento departamento = q.setParameter("c", clave).getSingleResult();
            if (departamento != null) {
                flag = true;
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public int validarNumero() {
        boolean flag = false;
        int numero = 0;
        do {
            try {
                numero = leer.nextInt();
                flag = true;
            } catch (Exception e) {
                System.out.print("Solo números: ");
                leer.next();
            }
        } while (!flag);
        return numero;
    }




}
