import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

//JPA - JAVA PERSISTENCE API
//SIRVE PARA MAPEAR UN OBJETO

@Entity
public class Alumno implements Serializable {

    @Id @GeneratedValue @OneToMany(cascade = CascadeType.ALL)
    private long id;

    @OneToMany(cascade = CascadeType.ALL)
    private String matricula;

    @OneToMany(cascade = CascadeType.ALL)
    private String nombre;

    @OneToMany(cascade = CascadeType.ALL)
    private Date fechaN;

    public Alumno() {
    }

    public Alumno(String matricula, String nombre, Date fechaN) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.fechaN = fechaN;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaN() {
        return fechaN;
    }

    public void setFechaN(Date fechaN) {
        this.fechaN = fechaN;
    }

    @Override
    public String toString() {
        return "Alumno{" +
                "id=" + id +
                ", matricula='" + matricula + '\'' +
                ", nombre='" + nombre + '\'' +
                ", fechaN=" + fechaN +
                '}';
    }
}
