import javax.persistence.*;
import java.util.List;

@Entity
public class Materia {

    @Id
    @GeneratedValue
    private long id;

    @OneToMany(cascade = CascadeType.ALL)
    private String clave;

    @OneToMany(cascade = CascadeType.ALL)
    private String nombre;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Alumno> lista;

    public Materia() {
    }

    public Materia(String clave, String nombre, List<Alumno> lista) {
        this.clave = clave;
        this.nombre = nombre;
        this.lista = lista;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Alumno> getLista() {
        return lista;
    }

    public void setLista(List<Alumno> lista) {
        this.lista = lista;
    }

    @Override
    public String toString() {
        return "Materia{" +
                "id=" + id +
                ", clave='" + clave + '\'' +
                ", nombre='" + nombre + '\'' +
                ", lista=" + lista +
                '}';
    }
}
