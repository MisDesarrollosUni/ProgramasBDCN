import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("object.odb");
        EntityManager em = emf.createEntityManager();
        Metodos m = new Metodos();
        Scanner input = new Scanner(System.in);
        input.useDelimiter("\n");
        m.menuGeneral(input,em);
    }
}
