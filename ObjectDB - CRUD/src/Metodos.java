import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Metodos {

    public List<Alumno> consultarTodos(EntityManager em){
        Query q = em.createQuery("SELECT a FROM Alumno a");
        List<Alumno> lista = q.getResultList();
        return lista;
    }

    public List<Materia> consultarTodasMaterias(EntityManager em){
        Query q = em.createQuery("SELECT m FROM Materia m");
        List<Materia> lista = q.getResultList();
        return lista;
    }

    public void eliminarNulos(EntityManager em, String matricula){
        Alumno alumno = consultarAlumno(em, matricula);
        Query q = em.createQuery("SELECT m FROM Materia m ");
        List<Materia> lista = q.getResultList();
        for (Materia m: lista) {
            List<Alumno> listAlumnos = consultarAlumnosMateria(em,m.getClave());
            for (Alumno a: listAlumnos) {
                if(alumno.getMatricula().equals(a.getMatricula())){
                    eliminarAlumnoMateria(em, matricula, m.getClave());
                    break;
                }
            }
        }
    }

    public Alumno consultarAlumno(EntityManager em, String matricula){
        Query q = em.createQuery("SELECT a FROM Alumno a WHERE a.matricula = : mat");
        Alumno resultado = (Alumno) q.setParameter("mat",matricula).getSingleResult();
        return resultado;
    }

    public Materia consultarMateria(EntityManager em, String clave){
        Query q = em.createQuery("SELECT m FROM Materia m WHERE m.clave = : cl");
        Materia resultado = (Materia) q.setParameter("cl",clave).getSingleResult();
        return resultado;
    }

    public List<Alumno> consultarAlumnosMateria(EntityManager em, String clave){
        Query q = em.createQuery("SELECT m FROM Materia a JOIN a.lista m where a.clave = : cl");
        List<Alumno> lista = q.setParameter("cl",clave).getResultList();
        return lista;
    }

    public void actualizarAlumno(EntityManager em, String matricula, Scanner input){
        //Primero tengo que buscar al alumno que quiero actualizar
        Alumno encontrar = consultarAlumno(em,matricula);
        //Luego que lo encuentre lo paso al metodo find para que lo busque y lo actualice alv jajaja XD
        Alumno alumno = em.find(Alumno.class,encontrar);
        System.out.println("Ingresa el nuevo nombre: ");
        String nombre = input.next();
        System.out.println("Ingresa la nueva fecha de nacimiento: ");
        String fechaN = input.next();
        em.getTransaction().begin();
        alumno.setNombre(nombre);
        alumno.setFechaN(Date.valueOf(fechaN));
        em.getTransaction().commit();
        System.out.println("Alumno actualizado");
    }


    public boolean verificar(EntityManager em, String matricula){
        try {
            TypedQuery<String> q = em.createQuery("SELECT a.matricula FROM Alumno a where a.matricula = : mat", String.class);
            String resultado = q.setParameter("mat",matricula).getSingleResult();
            if (resultado.equals(matricula)){
                return true;
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean verificarMateria(EntityManager em, String clave){
        try {
            Query q = em.createQuery("SELECT m.clave FROM Materia m where m.clave = : cla");
            String resultado = (String) q.setParameter("cla",clave).getSingleResult();
            if (resultado.equals(clave)){
                return true;
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean verificarAlumnoMateria(EntityManager em, String matricula, String clave){
        try{
            Query q = em.createQuery("SELECT m.matricula FROM Materia a JOIN a.lista m where a.clave = : cl AND m.matricula = : mat");
            String resultado = (String) q.setParameter("cl",clave).setParameter("mat",matricula).getSingleResult();
            if (resultado.equals(matricula)){
                return true;
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public Alumno consultarAlumnoMateria(EntityManager em, String matricula, String clave){
        // Materia resultado = (Materia) q.setParameter("cl",clave).getSingleResult();
       Query q = em.createQuery("SELECT m FROM Materia a JOIN a.lista m where a.clave = : cl AND m.matricula = : mat");
       Alumno resultado = (Alumno) q.setParameter("cl",clave).setParameter("mat",matricula).getSingleResult();
       return resultado;
    }

    public void eliminarAlumno(EntityManager em, String matricula){
        Alumno encontrar = consultarAlumno(em,matricula);
        Alumno alumno = em.find(Alumno.class,encontrar);
        em.getTransaction().begin();
        em.remove(alumno);
        em.getTransaction().commit();
        System.out.println("Alumno eliminado");
    }

    public void eliminarMateria(EntityManager em, String clave){
        Materia encontrar = consultarMateria(em, clave);
        Materia materia = em.find(Materia.class,encontrar);
        em.getTransaction().begin();
        em.remove(materia);
        em.getTransaction().commit();
        System.out.println("Materia eliminada");
    }


    public void eliminarAlumnoMateria(EntityManager em, String matricula, String clave){
        List<Alumno> listaAlumnos = consultarAlumnosMateria(em,clave);
        Alumno alumno = consultarAlumnoMateria(em,matricula,clave);
        Materia materia = consultarMateria(em,clave);
        listaAlumnos.remove(alumno);
        em.getTransaction().begin();
        materia.setLista(listaAlumnos);
        em.getTransaction().commit();
        System.out.println("Alumno eliminado");
    }

    public void insertarMateria(EntityManager em, Scanner input){
        System.out.println("Ingrese la clave: ");
        String clave = input.next();
        System.out.println("Ingresa el nombre de la materia: ");
        String nombre = input.next();
        List<Alumno> listaNueva = new ArrayList<>();
        em.getTransaction().begin();
        Materia materia = new Materia(clave,nombre,listaNueva);
        em.persist(materia);
        em.getTransaction().commit();
        System.out.println("================================================");
        System.out.println("Materia registrada");
        System.out.println("================================================");
    }


    public void insertarAlumnoMateria(EntityManager em, String matricula, String clave){
        List<Alumno> listaAlumnos = consultarAlumnosMateria(em,clave);
        Alumno alumno = consultarAlumno(em,matricula);
        Materia materia = consultarMateria(em,clave);
        listaAlumnos.add(alumno);
        em.getTransaction().begin();
        materia.setLista(listaAlumnos);
        em.getTransaction().commit();
        System.out.println("Alumno registrado");
    }

    public void insertarAlumno(Scanner input, EntityManager em){
        System.out.println("Ingresa la matricula: ");
        String matricula = input.next();
        System.out.println("Ingresa el nombre completo: ");
        String nombre = input.next();
        System.out.println("Ingresa la fecha de nacimiento: ");
        String fechaN = input.next();
        em.getTransaction().begin();
        Alumno alumno = new Alumno(matricula,nombre, Date.valueOf(fechaN));
        em.persist(alumno);
        em.getTransaction().commit();
        System.out.println("================================================");
        System.out.println("Alumno registrado");
        System.out.println("================================================");
    }
    
    public void menuAlumno(Scanner input, EntityManager em){
        int opc = 0;
        do {
            System.out.println("[1] Registrar alumno");
            System.out.println("[2] Consultar todos los alumnos");
            System.out.println("[3] Consultar un alumno");
            System.out.println("[4] Editar un alumno");
            System.out.println("[5] Eliminar un alumno");
            System.out.println("[6] Regresar al menú");
            System.out.println("Selecione una opción: ");
            opc = input.nextInt();
            switch (opc){
                case 1:
                    insertarAlumno(input,em);
                    break;
                case 2:
                    List<Alumno> lista = consultarTodos(em);
                    for (Alumno alumno:lista) {
                        System.out.println("================================================");
                        System.out.println("Nombre: " + alumno.getNombre());
                        System.out.println("Matricula: " + alumno.getMatricula());
                        System.out.println("Fecha de Nacimiento: " + alumno.getFechaN());
                        System.out.println("================================================");
                    }
                    break;
                case 3:
                        System.out.println("Ingrese la matricula: ");
                        String matricula = input.next();
                       if (verificar(em,matricula)){
                           Alumno alumno = consultarAlumno(em,matricula);
                           System.out.println("================================================");
                           System.out.println("Nombre: " + alumno.getNombre());
                           System.out.println("Matricula: " + alumno.getMatricula());
                           System.out.println("Fecha de Nacimiento: " + alumno.getFechaN());
                           System.out.println("================================================");
                       }else {
                           System.out.println("================================================");
                           System.out.println("El alumno con esa matricula no existe");
                           System.out.println("================================================");
                       }
                    break;
                case 4:
                    System.out.println("Ingrese la matricula: ");
                    String mat = input.next();
                    if (verificar(em,mat)){
                        actualizarAlumno(em,mat,input);
                    }else {
                        System.out.println("================================================");
                        System.out.println("El alumno con esa matricula no existe");
                        System.out.println("================================================");
                    }
                    break;
                case 5:
                    System.out.println("Ingrese la matricula: ");
                    String m = input.next();
                    if (verificar(em,m)){
                        eliminarNulos(em, m);
                        eliminarAlumno(em,m);
                    }else{
                        System.out.println("================================================");
                        System.out.println("El alumno con esa matricula no existe");
                        System.out.println("================================================");
                    }
                    break;
                case 6:
                    System.out.println("");
                    break;
                default:
                    System.out.println("Ingrese una opción valida");
                    break;
            }
        }while(opc!=6);
    }

    public void menuMateria(Scanner input, EntityManager em){
        int opc = 0;
        do {
            System.out.println("[1] Registrar materia");
            System.out.println("[2] Registrar alumno a una materia");
            System.out.println("[3] Consultar alumnos de una materia");
            System.out.println("[4] Eliminar alumno de una materia");
            System.out.println("[5] Consultar todas las materias");
            System.out.println("[6] Eliminar una materia");
            System.out.println("[7] Regresar al menú");
            System.out.println("Selecione una opción: ");
            opc = input.nextInt();
            switch (opc){
                case 1:
                    insertarMateria(em, input);
                    break;
                case 2:
                    System.out.println("Ingrese la clave de la materia: ");
                    String clave = input.next();
                    if (verificarMateria(em,clave)){
                        System.out.println("Ingrese la matricula del estudiante: ");
                        String matricula = input.next();
                        if (verificar(em,matricula)){
                            insertarAlumnoMateria(em,matricula,clave);
                        }else {
                            System.out.println("No existe ese alumno");
                        }
                    }else {
                        System.out.println("No existe esa materia");
                    }
                    break;
                case 3:
                    System.out.println("Ingrese la clave de la materia: ");
                    String cla = input.next();
                    if (verificarMateria(em,cla)){
                        List<Alumno> lista = consultarAlumnosMateria(em, cla);
                        for (Alumno alumno: lista) {
                            System.out.println("================================================");
                            System.out.println("Nombre: " + alumno.getNombre());
                            System.out.println("Matricula: " + alumno.getMatricula());
                            System.out.println("================================================");
                        }
                    }else {
                        System.out.println("================================================");
                        System.out.println("La materia con esa clave no existe");
                        System.out.println("================================================");
                    }
                    break;
                case 4:
                    System.out.println("Ingresa la clave de la materia");
                    String cl = input.next();
                    if (verificarMateria(em,cl)){
                        System.out.println("Ingrese la matricula del alumno: ");
                        String mat = input.next();
                        if (verificarAlumnoMateria(em,mat,cl)){
                            eliminarAlumnoMateria(em,mat,cl);
                        }else{
                            System.out.println("================================================");
                            System.out.println("Alumno no registrado o encontrado");
                            System.out.println("================================================");
                        }
                    }else {
                        System.out.println("================================================");
                        System.out.println("La materia con esa clave no existe");
                        System.out.println("================================================");
                    }
                    break;
                case 5:
                    List<Materia> lista = consultarTodasMaterias(em);
                    for (Materia materia:lista) {
                        System.out.println("================================================");
                        System.out.println("Nombre: " + materia.getNombre());
                        System.out.println("Matricula: " + materia.getClave());
                        System.out.println("================================================");
                    }
                    break;
                case 6:
                    System.out.println("Ingrese la clave: ");
                    String clav = input.next();
                    if (verificarMateria(em,clav)){
                        eliminarMateria(em,clav);
                    }else{
                        System.out.println("================================================");
                        System.out.println("La materia con esa clave no existe");
                        System.out.println("================================================");
                    }
                    break;
                case 7:
                    System.out.println("");
                    break;
            }
        }while (opc!=7);
    }


    public void menuGeneral(Scanner input, EntityManager em){
        int opc = 0;
        do {
            System.out.println("[1] Alumno");
            System.out.println("[2] Materia");
            System.out.println("[3] Salir del programa");
            System.out.println("Seleccione una opción: ");
            opc = input.nextInt();
            switch (opc){
                case 1:
                    menuAlumno(input, em);
                    break;
                case 2:
                    menuMateria(input, em);
                    break;
                case 3:
                    System.out.println("HASTA LUEGO");
                    break;
            }
        }while (opc!=3);
    }
}
