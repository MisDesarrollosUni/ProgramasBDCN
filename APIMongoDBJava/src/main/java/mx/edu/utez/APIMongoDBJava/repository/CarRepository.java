package mx.edu.utez.APIMongoDBJava.repository;

import mx.edu.utez.APIMongoDBJava.document.Car;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.io.Serializable;

@Repository
public interface CarRepository extends MongoRepository<Car, Serializable> {}
