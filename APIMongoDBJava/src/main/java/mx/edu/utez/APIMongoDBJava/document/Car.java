package mx.edu.utez.APIMongoDBJava.document;

import com.mongodb.lang.NonNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document(collection = "cars")
@Data
@NoArgsConstructor
@ToString
public class Car implements Serializable {
    @Id @NonNull
    private String id;
    private String model;
    private String brand;
    private int year;
    private String color;
}
