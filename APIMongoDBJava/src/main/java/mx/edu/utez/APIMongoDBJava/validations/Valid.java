package mx.edu.utez.APIMongoDBJava.validations;

import mx.edu.utez.APIMongoDBJava.document.Car;

public class Valid {
    private final String CARACTERES = "áéíóúÁÉÍÓÚabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789. ";

    public boolean validarCampoTexto(String texto) {
        boolean flag = true;
        if (!texto.isEmpty()) {
            for (var i = 0; i < texto.length(); i++) {
                if (CARACTERES.indexOf(texto.charAt(i)) == -1) {
                    flag = false;
                    break;
                }
            }
        }
        return flag;
    }

    public boolean validation(Car car){
        boolean flag = false;
        try {
            if(validarCampoTexto(car.getModel()) && validarCampoTexto(car.getBrand()) && validarCampoTexto(car.getColor())){
                flag = true;
            }
        }catch (Exception e){
            e.getStackTrace();
        }finally {
            return flag;
        }
    }

}
