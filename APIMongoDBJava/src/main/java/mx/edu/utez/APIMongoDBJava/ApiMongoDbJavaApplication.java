package mx.edu.utez.APIMongoDBJava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMongoDbJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiMongoDbJavaApplication.class, args);
	}

}
