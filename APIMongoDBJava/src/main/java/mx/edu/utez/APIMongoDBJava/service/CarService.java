package mx.edu.utez.APIMongoDBJava.service;

import mx.edu.utez.APIMongoDBJava.document.Car;
import mx.edu.utez.APIMongoDBJava.repository.CarRepository;
import mx.edu.utez.APIMongoDBJava.validations.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;
    private Valid valid = new Valid();

    public List<Car> getAll() {
        return carRepository.findAll();
    }

    public Optional<Car> getOne(String id) {
        return carRepository.findById(id);
    }

    public Optional<Car> insertOne(Car car) {
        if(valid.validation(car)){
            carRepository.save(car);
            return getOne(car.getId());
        }
        return Optional.empty();
    }

    public Optional<Car> updateOne(Car car) {
        try {
            if (getOne(car.getId()).get() != null && valid.validation(car)) {
                carRepository.save(car);
                return getOne(car.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public boolean removeOne(String id) {
        try {
            if (getOne(id).get() != null) {
                carRepository.deleteById(id);
                return true;
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        return false;
    }

}
