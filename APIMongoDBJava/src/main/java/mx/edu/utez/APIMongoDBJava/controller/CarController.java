package mx.edu.utez.APIMongoDBJava.controller;

import mx.edu.utez.APIMongoDBJava.document.Car;
import mx.edu.utez.APIMongoDBJava.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class CarController {

    @Autowired
    private CarService carService;

    @GetMapping("/cars")
    public ResponseEntity<List<Car>> getAll() {
        try {
            return new ResponseEntity<>(carService.getAll(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("/car/{id}")
    public ResponseEntity<Optional<Car>> getOne(@PathVariable("id") String id) {
        try {
            return new ResponseEntity<>(carService.getOne(id), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping("/car")
    public ResponseEntity<Optional<Car>> insertOne(@RequestBody Car car) {
        try {
            Optional<Car> resul = carService.insertOne(car);
            if(!resul.isEmpty()) return new ResponseEntity<>(resul, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PutMapping("/car")
    public ResponseEntity<Optional<Car>> updateOne(@RequestBody Car car) {
        try {
            Optional<Car> resul = carService.updateOne(car);
            if(!resul.isEmpty()) return new ResponseEntity<>(resul, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @DeleteMapping("/car/{id}")
    public ResponseEntity<Boolean> deleteOne(@PathVariable("id") String id) {
        try {
            return new ResponseEntity<>(carService.removeOne(id), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
