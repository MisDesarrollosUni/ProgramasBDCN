package mx.edu.utez.controller;

import mx.edu.utez.model.Estado;
import mx.edu.utez.model.Pais;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Metodos {

    final String CARACTERES = "áéíóúÁÉÍÓÚabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ ";
    private Scanner leer = new Scanner(System.in);
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("base_paises.odb");
    private EntityManager em = emf.createEntityManager();

    public void menu() {
        int opc, opc2;
        long poblacion = 0;
        String nombrePais, nombreCont = null, nombreEstado, nombreNuevo;
        do {
            System.out.println("\n************************");
            System.out.println("\tMENÚ ACCIONES");
            System.out.println("1. Registrar País\n2. Registrar Estado\n3. Eliminar País\n4. Eliminar Estado\n5. Modificar País\n6. Modificar Estado\n7. Agregar Estado a País");
            System.out.println("\n\tMENÚ CONSULTAS");
            System.out.println("8. Cantidad paises por contienente\n9. Cantidad estados por contienente\n10. Paises con poblacion > 10000000 personas \n11. Estados con población menor a 100000 personas");
            System.out.println("12. Salir");
            System.out.print("Elegir: ");
            opc = validarNumero();
            switch (opc) {
                case 1:
                    System.out.print("Nombre del pais");
                    nombrePais = validarNombre();
                    if (!existePais(nombrePais)) {
                        System.out.println("Seleccione contienente");
                        System.out.println("1. ASIA");
                        System.out.println("2. AMERICA");
                        System.out.println("3. AFRICA");
                        System.out.println("4. ANTÁRTIDA");
                        System.out.println("5. EUROPA");
                        System.out.println("6. OCEANÍA");
                        System.out.print("Eliga: ");
                        opc2 = validarNumero();
                        switch (opc2) {
                            case 1:
                                nombreCont = "ASIA";
                                break;
                            case 2:
                                nombreCont = "AMERICA";
                                break;
                            case 3:
                                nombreCont = "AFRICA";
                                break;
                            case 4:
                                nombreCont = "ANTARTIDA";
                                break;
                            case 5:
                                nombreCont = "EUROPA";
                                break;
                            case 6:
                                nombreCont = "OCEANIA";
                                break;
                            default:
                                System.out.println("NO EXISTE ESA OPCIÓN");
                        }
                        System.out.print("Inserte población: ");
                        poblacion = validarPoblacion();
                        if (insertarPais(new Pais(nombrePais, nombreCont, poblacion, new ArrayList<>()))) {
                            System.out.println("PAÍS INSERTADO");
                        } else {
                            System.out.println("PAÍS NO INSERTADO");
                        }
                    } else {
                        System.out.println("PAÍS EXISTENTE");
                    }
                    break;
                case 2:
                    System.out.print("Nombre del estado");
                    nombreEstado = validarNombre();
                    if (!existeEstado(nombreEstado)) {
                        System.out.print("Inserte población: ");
                        poblacion = validarPoblacion();
                        if (insertarEstado(new Estado(nombreEstado, poblacion))) {
                            System.out.println("ESTADO INSERTADO");
                        } else {
                            System.out.println("ESTADO NO INSERTADO");
                        }
                    } else {
                        System.out.println("ESTADO EXISTENTE");
                    }
                    break;
                case 3:
                    System.out.print("Nombre del pais");
                    nombrePais = validarNombre();
                    if (existePais(nombrePais)) {
                        System.out.println("1. SI");
                        System.out.println("2. NO");
                        System.out.print("Elige: ");
                        opc2 = validarNumero();
                        switch (opc2) {
                            case 1:
                                if (eliminarPais(nombrePais)) {
                                    System.out.println("PAÍS ELIMINADO");
                                } else {
                                    System.out.println("PAÍS NO ELIMINADO");
                                }
                                break;
                            case 2:
                                break;
                            default:
                                System.out.println("NO EXISTE ESA OPCIÓN");
                        }
                    } else {
                        System.out.println("PAÍS NO EXISTENTE");
                    }
                    break;
                case 4:
                    System.out.print("Nombre del estado");
                    nombreEstado = validarNombre();
                    if (existeEstado(nombreEstado)) {
                        System.out.println("1. SI");
                        System.out.println("2. NO");
                        System.out.print("Elige: ");
                        opc2 = validarNumero();
                        switch (opc2) {
                            case 1:
                                if (eliminarEstado(nombreEstado)) {
                                    System.out.println("ESTADO ELIMINADO");
                                } else {
                                    System.out.println("ESTADO NO ELIMINADO");
                                }
                                break;
                            case 2:
                                break;
                            default:
                                System.out.println("NO EXISTE ESA OPCIÓN");
                        }
                    } else {
                        System.out.println("ESTADO NO EXISTENTE");
                    }
                    break;
                case 5:
                    System.out.print("Nombre del pais");
                    nombrePais = validarNombre();
                    if (existePais(nombrePais)) {
                        System.out.print("Nuevo nombre del pais");
                        nombreNuevo = validarNombre();
                        if (modificarPais(nombrePais, nombreNuevo)) {
                            System.out.println("PAÍS MODIFICADO");
                        } else {
                            System.out.println("PAÍS NO MODIFICADO");
                        }
                    } else {
                        System.out.println("PAÍS NO EXISTENTE");
                    }
                    break;
                case 6:
                    System.out.print("Nombre del estado");
                    nombreEstado = validarNombre();
                    if (existeEstado(nombreEstado)) {
                        System.out.print("Nuevo nombre del estado");
                        nombreNuevo = validarNombre();
                        if (modificarEstado(nombreEstado, nombreNuevo)) {
                            System.out.println("ESTADO MODIFICADO");
                        } else {
                            System.out.println("ESTADO NO MODIFICADO");
                        }
                    } else {
                        System.out.println("ESTADO NO EXISTENTE");
                    }
                    break;
                case 7:
                    System.out.print("Nombre del pais");
                    nombrePais = validarNombre();
                    if (existePais(nombrePais)) {
                        System.out.print("Nombre del estado");
                        nombreEstado = validarNombre();
                        if (existeEstado(nombreEstado)) {
                            if (agregarEstadoPais(nombrePais, nombreEstado)) {
                                System.out.println("ESTADO AGREGADO A PAIS");
                            } else {
                                System.out.println("ESTADO NO AGREGADO A PAIS");
                            }
                        } else {
                            System.out.println("ESTADO NO EXISTENTE");
                        }
                    } else {
                        System.out.println("PAÍS NO EXISTENTE");
                    }
                    break;
                case 8:
                    consultarPaisesPorContienntes();
                    break;
                case 9:
                    consultarEstadosPorContinentes();
                    break;
                case 10:
                    paisesPoblacionMayor();
                    break;
                case 11:
                    paisesPoblacionMenor();
                    break;
                case 12:
                    System.out.println("\tBYE BYE");
                    break;
                default:
                    System.out.println("NO EXISTE ESA OPCIÓN");
            }
        } while (opc != 12);
    }

    public void paisesPoblacionMenor() {
        TypedQuery<Estado> q = em.createQuery("SELECT e FROM Estado e", Estado.class);
        List<Estado> estados = q.getResultList();
        if (!estados.isEmpty()) {
            for (Estado e : estados) {
                if (e.getPoblacion() < 100000) {
                    System.out.println(e);
                }
            }
        } else {
            System.out.println("AÚN NO HAY ESTADOS");
        }
    }

    public void paisesPoblacionMayor() {
        TypedQuery<Pais> q = em.createQuery("SELECT p FROM Pais p", Pais.class);
        List<Pais> paises = q.getResultList();
        if (!paises.isEmpty()) {
            for (Pais p : paises) {
                if (p.getPoblación() > 10000000) {
                    System.out.println(p);
                }
            }
        } else {
            System.out.println("AÚN NO HAY PAISES");
        }
    }

    public void consultarEstadosPorContinentes() {
        int contAsia = 0, contAme = 0, contAfri = 0, contAnti = 0, contEur = 0, contOce = 0;
        TypedQuery<Pais> q = em.createQuery("SELECT p FROM Pais p", Pais.class);
        List<Pais> paises = q.getResultList();
        if (!paises.isEmpty()) {
            for (Pais p : paises) {
                switch (p.getContinente()) {
                    case "ASIA":
                        contAsia += p.getEstados().size();
                        break;
                    case "AMERICA":
                        contAme += p.getEstados().size();
                        break;
                    case "AFRICA":
                        contAfri += p.getEstados().size();
                        break;
                    case "ANTARTIDA":
                        contAnti += p.getEstados().size();
                        break;
                    case "EUROPA":
                        contEur += p.getEstados().size();
                        break;
                    case "OCEANIA":
                        contOce += p.getEstados().size();
                        break;
                }
            }
            System.out.println("ESTADOS EN ASIA: " + contAsia);
            System.out.println("ESTADOS EN AMERICA: " + contAme);
            System.out.println("ESTADOS EN AFRICA: " + contAfri);
            System.out.println("ESTADOS EN ANTARTIDA: " + contAnti);
            System.out.println("ESTADOS EN EUROPA: " + contEur);
            System.out.println("ESTADOS EN OCEANIA: " + contOce);
        } else {
            System.out.println("AÚN NO HAY PAISES");
        }
    }

    public void consultarPaisesPorContienntes() {
        int contAsia = 0, contAme = 0, contAfri = 0, contAnti = 0, contEur = 0, contOce = 0;
        TypedQuery<Pais> q = em.createQuery("SELECT p FROM Pais p", Pais.class);
        List<Pais> paises = q.getResultList();
        if (!paises.isEmpty()) {
            for (Pais p : paises) {
                switch (p.getContinente()) {
                    case "ASIA":
                        contAsia++;
                        break;
                    case "AMERICA":
                        contAme++;
                        break;
                    case "AFRICA":
                        contAfri++;
                        break;
                    case "ANTARTIDA":
                        contAnti++;
                        break;
                    case "EUROPA":
                        contEur++;
                        break;
                    case "OCEANIA":
                        contOce++;
                        break;
                }
            }
            System.out.println("PAISES EN ASIA: " + contAsia);
            System.out.println("PAISES EN AMERICA: " + contAme);
            System.out.println("PAISES EN AFRICA: " + contAfri);
            System.out.println("PAISES EN ANTARTIDA: " + contAnti);
            System.out.println("PAISES EN EUROPA: " + contEur);
            System.out.println("PAISES EN OCEANIA: " + contOce);
        } else {
            System.out.println("AÚN NO HAY PAISES");
        }
    }

    private boolean agregarEstadoPais(String nombrePais, String nombreEstado) {
        boolean flag = false;
        try {
            Pais pais = consultarPais(nombrePais);
            Estado estado = consultarEstado(nombreEstado);
            em.getTransaction().begin();
            pais.getEstados().add(estado);
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public boolean modificarEstado(String nombreAntiguo, String nombreNuevo) {
        boolean flag = false;
        try {
            Estado estado = consultarEstado(nombreAntiguo);
            em.getTransaction().begin();
            estado.setNombre(nombreNuevo);
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public boolean modificarPais(String nombreAntiguo, String nombreNuevo) {
        boolean flag = false;
        try {
            Pais pais = consultarPais(nombreAntiguo);
            em.getTransaction().begin();
            pais.setNombre(nombreNuevo);
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    private boolean eliminarEstado(String nombre) {
        boolean flag = false;
        try {
            Estado estado = consultarEstado(nombre);
            em.getTransaction().begin();
            em.remove(estado);
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    private Estado consultarEstado(String nombre) {
        Estado estado = null;
        try {
            TypedQuery<Estado> q = em.createQuery("SELECT e FROM Estado e WHERE e.nombre = :estado", Estado.class);
            estado = q.setParameter("estado", nombre).getSingleResult();
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return estado;
        }
    }

    public boolean eliminarPais(String nombre) {
        boolean flag = false;
        try {
            Pais pais = consultarPais(nombre);
            em.getTransaction().begin();
            em.remove(pais);
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    private Pais consultarPais(String nombre) {
        Pais pais = null;
        try {
            TypedQuery<Pais> q = em.createQuery("SELECT p FROM Pais p WHERE p.nombre = :pais", Pais.class);
            pais = q.setParameter("pais", nombre).getSingleResult();
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return pais;
        }
    }

    public boolean existeEstado(String nombre) {
        boolean flag = false;
        try {
            TypedQuery<Estado> q = em.createQuery("SELECT e FROM Estado e WHERE e.nombre = :estado", Estado.class);
            Estado estado = q.setParameter("estado", nombre).getSingleResult();
            if (estado != null) {
                flag = true;
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public boolean existePais(String nombre) {
        boolean flag = false;
        try {
            TypedQuery<Pais> q = em.createQuery("SELECT p FROM Pais p WHERE p.nombre = :pais", Pais.class);
            Pais pais = q.setParameter("pais", nombre).getSingleResult();
            if (pais != null) {
                flag = true;
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public boolean insertarEstado(Estado estado) {
        boolean flag = false;
        try {
            em.getTransaction().begin();
            em.persist(estado);
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public boolean insertarPais(Pais pais) {
        boolean flag = false;
        try {
            em.getTransaction().begin();
            em.persist(pais);
            em.getTransaction().commit();
            flag = true;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            return flag;
        }
    }

    public String validarNombre() {
        leer.useDelimiter("\n");
        String texto;
        boolean flag;
        do {
            flag = false;
            System.out.print(": ");
            texto = leer.next();
            if (!texto.isEmpty()) {
                for (int i = 0; i < texto.length(); i++) {
                    if (CARACTERES.indexOf(texto.charAt(i)) == -1) {
                        flag = true;
                        break;
                    }
                }
            } else {
                System.out.println("*Carácteres no válidos");
            }
        } while (flag);
        return texto;
    }

    public int validarPoblacion() {
        boolean flag = false;
        int numero = 0;
        do {
            try {
                numero = leer.nextInt();
                if (numero > 0) {
                    flag = true;
                }
            } catch (Exception e) {
                System.out.print("Solo población positiva: ");
                leer.next();
            }
        } while (!flag);
        return numero;
    }

    public int validarNumero() {
        boolean flag = false;
        int numero = 0;
        do {
            try {
                numero = leer.nextInt();
                flag = true;
            } catch (Exception e) {
                System.out.print("Solo números: ");
                leer.next();
            }
        } while (!flag);
        return numero;
    }
}

