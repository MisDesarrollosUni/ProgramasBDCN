package mx.edu.utez.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Pais implements Serializable {

    @Id @GeneratedValue
    private long id;

    private String nombre;
    private String continente;
    private long población;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Estado> estados;

    public Pais(String nombre, String continente, long población, List<Estado> estados) {
        this.nombre = nombre;
        this.continente = continente;
        this.población = población;
        this.estados = estados;
    }

    public Pais() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContinente() {
        return continente;
    }

    public void setContinente(String continente) {
        this.continente = continente;
    }

    public long getPoblación() {
        return población;
    }

    public void setPoblación(long población) {
        this.población = población;
    }

    public List<Estado> getEstados() {
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    @Override
    public String toString() {
        return "Pais{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", continente='" + continente + '\'' +
                ", población=" + población +
                ", estados=" + estados +
                '}';
    }
}
