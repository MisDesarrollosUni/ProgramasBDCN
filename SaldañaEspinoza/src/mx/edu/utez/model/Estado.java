package mx.edu.utez.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Estado implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String nombre;
    private long poblacion;

    public Estado() {
    }

    public Estado(String nombre, long poblacion) {
        this.nombre = nombre;
        this.poblacion = poblacion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(long poblacion) {
        this.poblacion = poblacion;
    }

    @Override
    public String toString() {
        return "Estado{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", poblacion=" + poblacion +
                '}';
    }
}
